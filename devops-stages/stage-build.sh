#!/bin/sh
#
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set -ex

rm -rf dist deb_dist osm*.tar.gz *.egg-info .eggs

for p in "osm_webhook_translator"; do 
    rm -rf $p/dist $p/deb_dist $p/osm*.tar.gz $p/*.egg-info $p/.eggs
done

mkdir -p deb_dist 
mkdir -p osm_webhook_translator/deb_dist

PACKAGES="
dist_ng_sa
dist_webhook_translator"

TOX_ENV_LIST="$(echo $PACKAGES | sed "s/ /,/g")"

tox -e $TOX_ENV_LIST

# Copying packages
# Webhook Translator
cp osm_webhook_translator/deb_dist/python3-osm-webhook-translator_*.deb deb_dist/

